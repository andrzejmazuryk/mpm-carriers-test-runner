echo "Configure Lighttpd and Start lighttpd server"
uuid=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
echo "$uuid" > /var/www/localhost/htdocs/health && lighttpd -f /etc/lighttpd/lighttpd.conf