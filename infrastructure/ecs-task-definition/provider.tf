provider "aws" {
  profile             = "${module.account.aws_profile}"
  region              = "${var.aws_region}"
  allowed_account_ids = ["${module.account.aws_account_id}"]
  version             = "~> 1.15"
}
