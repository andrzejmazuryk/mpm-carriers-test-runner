resource "aws_ecs_task_definition" "task_definition" {
  family                = "${local.service_name_prefix_underscore}"
  container_definitions = "${data.template_file.container_definition.rendered}"
  network_mode = "awsvpc"
  cpu="${var.fargate_cpu}"
  memory="${var.fargate_memory}"
  task_role_arn = "${local.taskRole}"
  execution_role_arn = "${local.taskExecutionRole}"
  requires_compatibilities = ["FARGATE"]
  tags = "${merge(local.tags, map("Name", "${local.service_name_prefix_underscore}"), map("mp:server-role", "ecs"))}"
}