locals {
    taskRole = "${data.terraform_remote_state.iam.ecs_task_role}"
    taskExecutionRole = "${data.terraform_remote_state.iam.ecs_task_execution_role}"
    DOCKERHUB_CREDENTIALS_ARN = "${data.aws_secretsmanager_secret.dockerhub.arn}"
    service_name_prefix_underscore = "${var.aws_env}_${var.aws_region}_${var.product}_${var.service}_${var.customer}"
    bucket = "${var.aws_env}-${var.aws_region}-terraform"
    s3_path = "aws/${var.aws_env}/${var.product}/${var.service}/${var.customer}"
}