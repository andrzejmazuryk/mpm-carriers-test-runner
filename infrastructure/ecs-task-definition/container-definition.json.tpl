[
    {
      "name": "${CONTAINER_NAME}",
      "image": "${DOCKER_IMAGE}",
      "repositoryCredentials": {
        "credentialsParameter": "${DOCKERHUB_CREDENTIALS_ARN}"
      },
      "essential": true,
      "portMappings": [
        {
          "containerPort": 80,
          "hostPort": 80
        },
        {
          "containerPort": 443,
          "hostPort": 443
        }
      ],
      "environment" : [
        { "name" : "projects_list", "value" : "${PROJECTS_LIST}" },
        { "name" : "env", "value" : "${ENV}" },
        { "name" : "dbLogJobIdGuid", "value" : "${DB_LOG_JOB_ID_GUID}" },
        { "name" : "enable_soap_logs", "value" : "${ENABLE_SOAP_LOGS}" }
      ],
      "logConfiguration": {
        "logDriver": "awslogs", 
        "options": {
          "awslogs-group" : "${AWSLOGS_GROUP}",
          "awslogs-region" : "eu-west-1",
          "awslogs-stream-prefix" : "ecs"
        }
      }
    }
  ]