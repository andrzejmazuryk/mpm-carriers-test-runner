data "terraform_remote_state" "iam" {
  backend   = "s3"
  config {
    bucket  = "${local.bucket}"
    key     = "${local.s3_path}/iam/terraform.tfstate"
    region  = "${var.aws_region}"
    profile = "${var.aws_account}"
  }
}

data "aws_secretsmanager_secret" "dockerhub" {
  name = "${var.aws_account}_${var.aws_region}_core_dockerhub_login"
}