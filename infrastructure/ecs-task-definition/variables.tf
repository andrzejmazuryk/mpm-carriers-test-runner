
variable "fargate_cpu" { default = 1024 }
variable "fargate_memory" { default = 4096 }

variable "DOCKER_IMAGE" {
  default="metapackltd/mpm-modulestests:latest"
}

variable "CONTAINER_NAME" {
  default = "mpm-modulestests"
}

variable "PROJECTS_LIST" {
  default = ""
}

variable "ENV" {
  default = ""
}

variable "DB_LOG_JOB_ID_GUID" {
  default = ""
}

variable "ENABLE_SOAP_LOGS" {
  default = "0"
}