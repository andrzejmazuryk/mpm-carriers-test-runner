data "template_file" "container_definition" {
  template = "${file("${path.module}/container-definition.json.tpl")}"
  vars = {
    AWSLOGS_GROUP = "/ecs/${var.aws_env}_${var.aws_region}_${var.product}_${var.service}_${var.customer}"
    DOCKER_IMAGE = "${var.DOCKER_IMAGE}"
    DOCKERHUB_CREDENTIALS_ARN = "${local.DOCKERHUB_CREDENTIALS_ARN}"
    CONTAINER_NAME = "${var.CONTAINER_NAME}"
    PROJECTS_LIST = "${var.PROJECTS_LIST}"
    ENV = "${var.ENV}"
    DB_LOG_JOB_ID_GUID = "${var.DB_LOG_JOB_ID_GUID}"
    ENABLE_SOAP_LOGS = "${var.ENABLE_SOAP_LOGS}"
  }
}