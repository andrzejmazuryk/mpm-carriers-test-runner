output "arn_http" {
  value = "${aws_lb_target_group.tg_http.arn}"
}

output "arn_suffix_http" {
  value = "${aws_lb_target_group.tg_http.arn_suffix}"
}