data "terraform_remote_state" "manage_vpc" {
  backend   = "s3"
  config {
    bucket  = "${var.aws_account}-${var.aws_region}-terraform"
    key     = "aws/core/manage/shd/vpc/terraform.tfstate"
    region  = "${var.aws_region}"
    profile = "${var.aws_account}"
  }
}