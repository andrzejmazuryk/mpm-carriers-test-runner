resource "aws_lb_target_group" "tg_http" {
  name     = "${var.aws_env}-${var.aws_region}-${var.product}-${var.service}-tg-http"
  port     = 80
  protocol = "HTTP"
  target_type = "ip"
  vpc_id   = "${local.vpc_id}"
  tags     = "${merge(local.tags, map("Name", "${var.aws_env}_${var.aws_region}_${var.product}_${var.service}_tg-http"))}"

  health_check {
    protocol            = "HTTP"
    path                = "/health"
    port                = "traffic-port"
    healthy_threshold   = 2
    unhealthy_threshold = 10
    timeout             = 10
    interval            = 60
    matcher             = "200"
  }
}