data "terraform_remote_state" "alb" {
  backend   = "s3"
  config {
    bucket  = "${var.aws_env}-${var.aws_region}-terraform"
    key     = "aws/${var.aws_env}/${var.product}/${var.service}/${var.customer}/alb/terraform.tfstate"
    region  = "${var.aws_region}"
    profile = "${var.aws_account}"
  }
}

data "aws_route53_zone" "zone" {
  name = "${var.aws_env}.aws.metapack.io"
}