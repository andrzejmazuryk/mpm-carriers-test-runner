resource "aws_route53_record" "tests" {
  zone_id = "${data.aws_route53_zone.zone.zone_id}"
  name    = "${var.service}.${var.aws_env}.aws.metapack.io"
  type    = "CNAME"
  ttl     = "300"
  records = ["${local.alb_int}"]
}