data "terraform_remote_state" "target-groups" {
 backend   = "s3"
 config {
   bucket  = "${local.bucket}"
   key     = "${local.s3_path}/target-groups/terraform.tfstate"
   region  = "${var.aws_region}"
   profile = "${var.aws_account}"
 }
}

data "terraform_remote_state" "alb" {
 backend   = "s3"
 config {
   bucket  = "${local.bucket}"
   key     = "${local.s3_path}/alb/terraform.tfstate"
   region  = "${var.aws_region}"
   profile = "${var.aws_account}"
 }
}