resource "aws_lb_listener" "http" {
  load_balancer_arn = "${local.alb_arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = "${local.target_groups_arn_http}"
  }
}