locals {
  target_groups_arn_http = "${data.terraform_remote_state.target-groups.arn_http}" 
  alb_arn = "${data.terraform_remote_state.alb.arn}"
  bucket = "${var.aws_env}-${var.aws_region}-terraform"
  s3_path = "aws/${var.aws_env}/${var.product}/${var.service}/${var.customer}"
}