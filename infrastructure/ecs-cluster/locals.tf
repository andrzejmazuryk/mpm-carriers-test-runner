locals {
    service_name_prefix_underscore = "${var.aws_env}_${var.aws_region}_${var.product}_${var.service}_${var.customer}"
}