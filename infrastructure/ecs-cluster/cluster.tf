resource "aws_ecs_cluster" "ecs" {
  name = "${local.service_name_prefix_underscore}"
  tags = "${merge(local.tags,map("Name", "${local.service_name_prefix_underscore}"),map("mp:server-role", "ecs"))}"
}