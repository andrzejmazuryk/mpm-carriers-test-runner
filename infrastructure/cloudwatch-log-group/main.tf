resource "aws_cloudwatch_log_group" "tests" {
  name = "/ecs/${var.aws_env}_${var.aws_region}_${var.product}_${var.service}_${var.customer}"
  retention_in_days = 30
  tags = "${local.tags}"
}