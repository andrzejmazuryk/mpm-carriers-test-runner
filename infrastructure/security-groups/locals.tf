locals {
  vpc_id = "${data.terraform_remote_state.manage_vpc.vpc_id}"
  vpc_cidr = "${data.terraform_remote_state.manage_vpc.vpc_cidr}" 
  service_name_prefix_underscore = "${var.aws_env}_${var.aws_region}_${var.product}_${var.service}_${var.customer}"
}