resource "aws_security_group" "app" {
  name              = "${local.service_name_prefix_underscore}_app"
  description       = "Security group for Tests APP"
  vpc_id            = "${local.vpc_id}"
  tags              = "${merge(local.tags, map("Name", "${local.service_name_prefix_underscore}_app"))}"
}

resource "aws_security_group_rule" "app-ingress-all" {
  security_group_id = "${aws_security_group.app.id}"
  type              = "ingress"
  from_port         = "0"
  to_port           = "0"
  protocol          = "-1"
  cidr_blocks       = ["${local.vpc_cidr}"]
}

resource "aws_security_group_rule" "app-egress" {
  security_group_id = "${aws_security_group.app.id}"
  type              = "egress"
  from_port         = "0"
  to_port           = "0"
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}