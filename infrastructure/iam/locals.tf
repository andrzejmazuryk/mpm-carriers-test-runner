locals {
  scoped_service_name = "${var.aws_env}_${var.aws_region}_${var.product}_${var.service}_${var.customer}"
  service_name_prefix_underscore = "${var.aws_env}_${var.aws_region}_${var.product}_${var.service}_${var.customer}"
  dockerhub_credentials_arn = "${data.aws_secretsmanager_secret.secret_manager.arn}"
  bitbucket_app_password_arn = "${data.aws_secretsmanager_secret.secret_id.arn}"
}