data "aws_iam_policy_document" "ecs_task_execution_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "ecs_task_execution_role" {
  name = "${local.service_name_prefix_underscore}_ecs-task-execution-role"
  assume_role_policy = "${data.aws_iam_policy_document.ecs_task_execution_role_policy.json}"
  tags   = "${merge(
    local.tags,
    map("Name", "${local.service_name_prefix_underscore}_ecs-task-execution-role"),
  )}"
}

resource "aws_iam_role_policy_attachment" "ecs_task_execution_role" {
  role = "${aws_iam_role.ecs_task_execution_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

data "aws_iam_policy_document" "docker_repo_credentials_access_role_policy" {
  statement {
    actions = [
      "kms:Decrypt",
      "secretsmanager:GetSecretValue"
    ]

    resources = [
      "${local.dockerhub_credentials_arn}"
    ]
  }
}

resource "aws_iam_policy" "docker_repo_credentials_access" {
  name        = "${local.service_name_prefix_underscore}_docker-repo-access-policy"
  description = "Policy to allow access to docker repository credentials"
  policy      = "${data.aws_iam_policy_document.docker_repo_credentials_access_role_policy.json}"
}

resource "aws_iam_role_policy_attachment" "ecs_task_execution_docker_repo_credential_access" {
  role = "${aws_iam_role.ecs_task_execution_role.name}"
  policy_arn = "${aws_iam_policy.docker_repo_credentials_access.arn}"
}