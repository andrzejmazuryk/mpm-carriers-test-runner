data "aws_secretsmanager_secret" "secret_manager" {
  name = "${var.aws_account}_${var.aws_region}_core_dockerhub_login"
}

data "aws_secretsmanager_secret" "secret_id" {
  name = "${var.aws_env}_${var.aws_region}_mpm_tests_shd_tests_bitbucket_app_password"
}