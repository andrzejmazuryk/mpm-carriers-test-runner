data "aws_iam_policy_document" "ecs_task_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "ecs_task_role" {
  name = "${local.service_name_prefix_underscore}_ecs-task-role"
  assume_role_policy = "${data.aws_iam_policy_document.ecs_task_role_policy.json}"
  tags   = "${merge(
     local.tags,
     map("Name", "${local.service_name_prefix_underscore}_ecs-task-role"),
   )}"
}

data "aws_iam_policy_document" "ssm_parameter_read_access_policy" {
  statement {
    actions = [
      "ssm:GetParametersByPath",
      "ssm:GetParameter",
      "ssm:GetParameters"
    ]

    resources = [
      "arn:aws:ssm:${var.aws_region}:*:parameter/",
      "arn:aws:ssm:${var.aws_region}:*:parameter/*"
    ]
  }
}

resource "aws_iam_policy" "ssm_parameter_read_access" {
  name        = "${local.service_name_prefix_underscore}_ssm-parameter-read-policy"
  description = "Policy to allow read parameters from parameter store"
  policy      = "${data.aws_iam_policy_document.ssm_parameter_read_access_policy.json}"
}

resource "aws_iam_role_policy_attachment" "ecs_task_docker_repo_credential_access" {
  role = "${aws_iam_role.ecs_task_role.name}"
  policy_arn = "${aws_iam_policy.ssm_parameter_read_access.arn}"
}

data "aws_iam_policy_document" "bitbucket_app_password_policy" {
  statement {
    actions = [
      "kms:Decrypt",
      "secretsmanager:GetSecretValue"
    ]

    resources = [
      "${local.bitbucket_app_password_arn}"
    ]
  }
}

resource "aws_iam_policy" "bitbucket_app_password_access" {
  name = "${local.scoped_service_name}_bitbucket-app-password-policy"
  description = "Policy to allow get bitbucket app password"
  policy = "${data.aws_iam_policy_document.bitbucket_app_password_policy.json}"
}

resource "aws_iam_role_policy_attachment" "exc_task_execution_bitbucket_app_password_access" {
  role = "${aws_iam_role.ecs_task_role.name}"
  policy_arn = "${aws_iam_policy.bitbucket_app_password_access.arn}"
}