resource "aws_iam_role" "app" {
  name = "${local.service_name_prefix_underscore}_app"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF

  tags = "${local.tags}"
}

resource "aws_iam_policy" "s3" {
  name = "${local.service_name_prefix_underscore}_app-s3"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:*ListAllMyBuckets",
                "s3:ListAllBuckets",
                "s3:GetObjectVersion"
            ],
            "Resource": [
                "arn:aws:s3:::*"
            ]
        },
        {
            "Action": [
                "s3:GetObject",
                "s3:ListBucket",
                "s3:ListObjects",
                "s3:PutObject",
                "s3:PutObjectAcl",
                "s3:GetObjectVersion"
            ],
            "Resource": [
                "arn:aws:s3:::${var.aws_env}-${var.aws_region}-${var.product}-${var.service}/*"
            ],
            "Effect": "Allow",
            "Sid": "Stmt1617361471023397"
        }
    ]
}
EOF
}

resource "aws_iam_policy" "ecs" {
  name = "${local.service_name_prefix_underscore}_app-ecs"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "ecs:CreateCluster",
                "ecs:DeregisterContainerInstance",
                "ecs:DiscoverPollEndpoint",
                "ecs:Poll",
                "ecs:RegisterContainerInstance",
                "ecs:StartTelemetrySession",
                "ecs:Submit*",
                "ecs:CreateCluster",
                "ecs:DeregisterContainerInstance",
                "ecs:DiscoverPollEndpoint",
                "ecs:Poll",
                "ecs:RegisterContainerInstance",
                "ecs:StartTelemetrySession",
                "ecs:UpdateContainerInstancesState",
                "ecs:Submit*",
                "ecr:GetAuthorizationToken",
                "ecr:BatchCheckLayerAvailability",
                "ecr:GetDownloadUrlForLayer",
                "ecr:BatchGetImage",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": [
                "*"
            ],
            "Effect": "Allow",
            "Sid": "Stmt1802111540599080"
        }
    ]
}
EOF
}

resource "aws_iam_policy" "ssm" {
  name = "${local.service_name_prefix_underscore}_app-ssm"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "ssm:GetParametersByPath",
                "ssm:GetParameters",
                "ssm:GetParameter"
            ],
            "Resource": [
                "arn:aws:ssm:*:*:parameter/*"
            ],
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_policy_attachment" "s3" {
  name       = "${local.service_name_prefix_underscore}_app-s3"
  roles      = ["${aws_iam_role.app.name}"]
  policy_arn = "${aws_iam_policy.s3.arn}"
}

resource "aws_iam_policy_attachment" "ecs" {
  name       = "${local.service_name_prefix_underscore}_app-ecs"
  roles      = ["${aws_iam_role.app.name}"]
  policy_arn = "${aws_iam_policy.ecs.arn}"
}

resource "aws_iam_policy_attachment" "ssm" {
  name       = "${local.service_name_prefix_underscore}_app-ssm"
  roles      = ["${aws_iam_role.app.name}"]
  policy_arn = "${aws_iam_policy.ssm.arn}"
}

resource "aws_iam_instance_profile" "app_instance_profile" {
  name = "${local.service_name_prefix_underscore}_app-instance-profile"
  path = "/"
  role = "${aws_iam_role.app.id}"
}