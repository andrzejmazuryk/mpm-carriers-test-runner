resource "aws_ecs_service" "tests-service" {
  name            = "${local.service_name_prefix_underscore}_tests-service"
  cluster         = "${local.ecs_cluster_id}"
  task_definition = "${local.service_name_prefix_underscore}"
  deployment_minimum_healthy_percent = "${var.deployment_minimum_healthy_percent}"
  deployment_maximum_percent = "${var.deployment_maximum_percent}"
  desired_count   = "${var.desired_count}"
  launch_type     = "FARGATE"
  
 load_balancer {
    target_group_arn = "${local.target_groups_arn_http}"
    container_name   = "${data.terraform_remote_state.task.container_name}"
    container_port   = "${var.port_http}"
  }

  network_configuration
  {
    subnets =["${local.subnet_a}", "${local.subnet_b}","${local.subnet_c}"]
    security_groups=["${local.sg_app}"]
  }
}