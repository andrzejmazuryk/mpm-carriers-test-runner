data "terraform_remote_state" "ecs-cluster" {
  backend   = "s3"
  config {
    bucket  = "${local.bucket}"
    key     = "${local.s3_path}/ecs-cluster/terraform.tfstate"
    region  = "${var.aws_region}"
    profile = "${var.aws_account}"
  }
}

data "terraform_remote_state" "target-groups" {
  backend   = "s3"
  config {
    bucket  = "${local.bucket}"
    key     = "${local.s3_path}/target-groups/terraform.tfstate"
    region  = "${var.aws_region}"
    profile = "${var.aws_account}"
  }
}

data "terraform_remote_state" "manage_vpc" {
  backend   = "s3"
  config {
    bucket  = "${var.aws_account}-${var.aws_region}-terraform"
    key     = "aws/core/manage/shd/vpc/terraform.tfstate"
    region  = "${var.aws_region}"
    profile = "${var.aws_account}"
  }
}

data "terraform_remote_state" "security-groups" {
  backend   = "s3"
  config {
    bucket  = "${local.bucket}"
    key     = "${local.s3_path}/security-groups/terraform.tfstate"
    region  = "${var.aws_region}"
    profile = "${var.aws_account}"
  }
}

data "terraform_remote_state" "task" {
  backend   = "s3"
  config {
    bucket  = "${local.bucket}"
    key     = "${local.s3_path}/ecs-task-definition/terraform.tfstate"
    region  = "${var.aws_region}"
    profile = "${var.aws_account}"
  }
}