locals {
  ecs_cluster_id = "${data.terraform_remote_state.ecs-cluster.name}"
  subnet_a = "${data.terraform_remote_state.manage_vpc.app_a}"
  subnet_b = "${data.terraform_remote_state.manage_vpc.app_b}"
  subnet_c = "${data.terraform_remote_state.manage_vpc.app_c}"
  sg_app = "${data.terraform_remote_state.security-groups.app}"
  target_groups_arn_http = "${data.terraform_remote_state.target-groups.arn_http}"
  service_name_prefix_underscore = "${var.aws_env}_${var.aws_region}_${var.product}_${var.service}_${var.customer}"
  bucket = "${var.aws_env}-${var.aws_region}-terraform"
  s3_path = "aws/${var.aws_env}/${var.product}/${var.service}/${var.customer}"
}