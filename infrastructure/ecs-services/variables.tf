variable "port_http" {
  default = 80
}

variable "deployment_minimum_healthy_percent" {
  default = 75
}

variable "deployment_maximum_percent" {
  default = 150
}

variable "desired_count" {
  default = 1
}
