resource "aws_secretsmanager_secret" "tests_bitbucket_app_password" {
  name = "${var.aws_env}_${var.aws_region}_${var.product}_${var.service}_${var.customer}_tests_bitbucket_app_password"
  recovery_window_in_days = 0
}

resource "aws_secretsmanager_secret_version" "tests_bitbucket_app_password" {
  secret_id     = "${aws_secretsmanager_secret.tests_bitbucket_app_password.id}"
  secret_string = "${var.bitbucket_app_password}"
}