resource "aws_security_group" "alb" {
  name              = "${local.service_name_prefix_underscore}_alb"
  description       = "Security group for Tests ALB"
  vpc_id            = "${local.vpc_id}"
  tags              = "${merge(local.tags, map("Name", "${local.service_name_prefix_underscore}_alb"))}"
}

resource "aws_security_group_rule" "alb-ingress-80" {
  security_group_id = "${aws_security_group.alb.id}"
  type              = "ingress"
  from_port         = "80"
  to_port           = "80"
  protocol          = "tcp"
  cidr_blocks       = ["${local.vpc_cidr}"]
}

resource "aws_security_group_rule" "alb-ingress-443" {
  security_group_id = "${aws_security_group.alb.id}"
  type              = "ingress"
  from_port         = "443"
  to_port           = "443"
  protocol          = "tcp"
  cidr_blocks       = ["${local.vpc_cidr}"]
}

resource "aws_security_group_rule" "alb-egress" {
  security_group_id = "${aws_security_group.alb.id}"
  type              = "egress"
  from_port         = "0"
  to_port           = "0"
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}