locals {
  subnet_a              = "${data.terraform_remote_state.manage_vpc.app_a}"
  subnet_b              = "${data.terraform_remote_state.manage_vpc.app_b}"
  subnet_c              = "${data.terraform_remote_state.manage_vpc.app_c}"
  vpc_id                = "${data.terraform_remote_state.manage_vpc.vpc_id}"
  vpc_cidr              = "${data.terraform_remote_state.manage_vpc.vpc_cidr}"
  service_name_prefix_underscore = "${var.aws_env}_${var.aws_region}_${var.product}_${var.service}_${var.customer}"
  service_name_prefix_dash = "${var.aws_env}-${var.product}-${var.service}-${var.customer}"    
}