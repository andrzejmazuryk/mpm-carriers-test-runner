resource "aws_lb" "alb" {
  name                        = "${local.service_name_prefix_dash}"
  internal                    = "${var.alb_private_access}"
  load_balancer_type          = "application"
  security_groups             = ["${aws_security_group.alb.id}"]
  subnets                     = ["${local.subnet_a}", "${local.subnet_b}", "${local.subnet_c}"]
  enable_deletion_protection  = false
  tags                        = "${merge(local.tags,map("Name", "${local.service_name_prefix_dash}"),map("mp:server-role", "alb"))}"

  lifecycle {
    prevent_destroy           = false
  }
}