#!groovy

pipeline {
	agent {
		label "${JENKINS_SLAVE_LABEL}"
	}
	options {
		ansiColor('xterm')
		timestamps()
	}
	stages {
		stage('Input parameters validation'){
			steps {
				wrap([$class: 'BuildUser']) {
                    script {
                        user = (env.BUILD_USER!=null) ? env.BUILD_USER : 'Jenkins'
                    }
                }
				slackSend(
                	baseUrl: "${SLACK_BASE_URL}", 
                	tokenCredentialId: "${SLACK_TOKEN_CREDENTIAL_ID}", 
                	color: '#D8D8D8', 
                	channel: "${SLACK_NOTIFICATION_CHANNEL}", 
                	message: """DEPLOYER: $user \n AWS ACCOUNT: ${AWS_ACCOUNT} \n STARTED: ${env.JOB_NAME} \n BUILD NUMBER: [#${env.BUILD_NUMBER}] \n MORE INFO AT: ${env.BUILD_URL}""")

				echo "Parameter name: TESTS_INFRASTRUCTURE_REPO_BRANCH_OR_COMMIT, value: ${params.TESTS_INFRASTRUCTURE_REPO_BRANCH_OR_COMMIT}"
				script {
					if (params.TESTS_INFRASTRUCTURE_REPO_BRANCH_OR_COMMIT != '')
					{
						echo 'All required parameters are valid.'
					} else {
						echo 'TESTS_INFRASTRUCTURE_REPO_BRANCH_OR_COMMIT is not set.'
						error fail
					}
				}
			}
		}
		stage('Checkout code'){
			steps {
				dir('metaform') {
					git url: 'git@github.com:metapack-infrastructure/Metaform.git', branch: "master", credentialsId: 'git-metaform'
				}
				dir(REPO_NAME){
					checkout([$class: 'GitSCM',
						branches: [[name: "${TESTS_INFRASTRUCTURE_REPO_BRANCH_OR_COMMIT}"]],
						userRemoteConfigs: [[credentialsId: 'bitbucket-credentials', url: '${SERVICE_REPO_ADDRESS}']]]
					)
				}
			}
		}
		stage('Destroy infrastructure stage 1'){
			parallel{
				stage('Destroy the ECS Services'){
					steps {
						withCredentials([
						sshUserPrivateKey(credentialsId: "git-metaform", keyFileVariable: 'keyfile')
						]) {
							sh """#!/bin/bash
							set -ex

							eval `ssh-agent`
							ssh-add \$keyfile

							export PATH=$PATH:$WORKSPACE/metaform/
							cd $WORKSPACE/$REPO_NAME/infrastructure/ecs-services

							[ -d ".terraform" ] && rm .terraform -r
							export TF_WARN_OUTPUT_ERRORS=1

							mf init --env $ENV --region $REGION --customer "\$CUSTOMER\" -input=false
							mf destroy -input=false -auto-approve
							""".stripIndent().trim()
						}
					}
				}
				stage('Destroy the Route53'){
					steps {
						withCredentials([
						sshUserPrivateKey(credentialsId: "git-metaform", keyFileVariable: 'keyfile')
						]) {
							sh """#!/bin/bash
							set -ex

							eval `ssh-agent`
							ssh-add \$keyfile

							export PATH=$PATH:$WORKSPACE/metaform/
							cd $WORKSPACE/$REPO_NAME/infrastructure/route53

							[ -d ".terraform" ] && rm .terraform -r
							export TF_WARN_OUTPUT_ERRORS=1

							mf init --env $ENV --region $REGION --customer "\$CUSTOMER\" -input=false
							mf destroy -input=false -auto-approve
							""".stripIndent().trim()
						}
					}
				}
				stage('Destroy the Task Definition'){
					steps {
						withCredentials([
						sshUserPrivateKey(credentialsId: "git-metaform", keyFileVariable: 'keyfile')
						]) {
							sh """#!/bin/bash
							set -ex

							eval `ssh-agent`
							ssh-add \$keyfile

							export PATH=$PATH:$WORKSPACE/metaform/
							cd $WORKSPACE/$REPO_NAME/infrastructure/ecs-task-definition

							[ -d ".terraform" ] && rm .terraform -r
							export TF_WARN_OUTPUT_ERRORS=1

							mf init --env $ENV --region $REGION --customer "\$CUSTOMER" -input=false
							mf destroy -input=false -auto-approve
							""".stripIndent().trim()
						}
					}
				}
				stage('Destroy the ALB listeners'){
					steps {
						withCredentials([
						sshUserPrivateKey(credentialsId: "git-metaform", keyFileVariable: 'keyfile')
						]) {
							sh """#!/bin/bash
							set -ex

							eval `ssh-agent`
							ssh-add \$keyfile

							export PATH=$PATH:$WORKSPACE/metaform/
							cd $WORKSPACE/$REPO_NAME/infrastructure/alb-listeners

							[ -d ".terraform" ] && rm .terraform -r
							export TF_WARN_OUTPUT_ERRORS=1

							mf init --env $ENV --region $REGION --customer "\$CUSTOMER" -input=false
							mf destroy -input=false -auto-approve
							""".stripIndent().trim()
						}
					}
				}
			}
		}
		stage('Destroy infrastructure stage 2'){
			parallel{
				stage('Destroy the IAM'){
					steps {
						withCredentials([
						sshUserPrivateKey(credentialsId: "git-metaform", keyFileVariable: 'keyfile')
						]) {
							sh """#!/bin/bash
							set -ex

							eval `ssh-agent`
							ssh-add \$keyfile

							export PATH=$PATH:$WORKSPACE/metaform/
							cd $WORKSPACE/$REPO_NAME/infrastructure/iam

							[ -d ".terraform" ] && rm .terraform -r
							export TF_WARN_OUTPUT_ERRORS=1

							mf init --env $ENV --region $REGION --customer "\$CUSTOMER" -input=false
							mf destroy -input=false -auto-approve
							""".stripIndent().trim()
						}
					}
				}
				stage('Destroy the ALB'){
					steps {
						withCredentials([
						sshUserPrivateKey(credentialsId: "git-metaform", keyFileVariable: 'keyfile')
						]) {
							sh """#!/bin/bash
							set -ex

							eval `ssh-agent`
							ssh-add \$keyfile

							export PATH=$PATH:$WORKSPACE/metaform/
							cd $WORKSPACE/$REPO_NAME/infrastructure/alb

							[ -d ".terraform" ] && rm .terraform -r
							export TF_WARN_OUTPUT_ERRORS=1

							mf init --env $ENV --region $REGION --customer "\$CUSTOMER" -input=false
							mf destroy -input=false -auto-approve
							""".stripIndent().trim()
						}
					}
				}
				stage('Destroy the Target Groups'){
					steps {
						withCredentials([
						sshUserPrivateKey(credentialsId: "git-metaform", keyFileVariable: 'keyfile')
						]) {
							sh """#!/bin/bash
							set -ex

							eval `ssh-agent`
							ssh-add \$keyfile

							export PATH=$PATH:$WORKSPACE/metaform/
							cd $WORKSPACE/$REPO_NAME/infrastructure/target-groups

							[ -d ".terraform" ] && rm .terraform -r
							export TF_WARN_OUTPUT_ERRORS=1

							mf init --env $ENV --region $REGION --customer "\$CUSTOMER" -input=false
							mf destroy -input=false -auto-approve
							""".stripIndent().trim()
						}
					}
				}
				stage('Destroy the ECS Cluster'){
					steps {
						withCredentials([
						sshUserPrivateKey(credentialsId: "git-metaform", keyFileVariable: 'keyfile')
						]) {
							sh """#!/bin/bash
							set -ex

							eval `ssh-agent`
							ssh-add \$keyfile

							export PATH=$PATH:$WORKSPACE/metaform/
							cd $WORKSPACE/$REPO_NAME/infrastructure/ecs-cluster

							[ -d ".terraform" ] && rm .terraform -r
							export TF_WARN_OUTPUT_ERRORS=1

							mf init --env $ENV --region $REGION --customer "\$CUSTOMER" -input=false
							mf destroy -input=false -auto-approve
							""".stripIndent().trim()
						}
					}
				}
				stage('Destroy the Security Group'){
					steps {
						withCredentials([
						sshUserPrivateKey(credentialsId: "git-metaform", keyFileVariable: 'keyfile')
						]) {
							sh """#!/bin/bash
							set -ex

							eval `ssh-agent`
							ssh-add \$keyfile

							export PATH=$PATH:$WORKSPACE/metaform/
							cd $WORKSPACE/$REPO_NAME/infrastructure/security-groups

							[ -d ".terraform" ] && rm .terraform -r
							export TF_WARN_OUTPUT_ERRORS=1

							mf init --env $ENV --region $REGION --customer "\$CUSTOMER" -input=false
							mf destroy -input=false -auto-approve
							""".stripIndent().trim()
						}
					}
				}
				stage('Destroy the Cloudwatch Log Group'){
					steps {
						withCredentials([
						sshUserPrivateKey(credentialsId: "git-metaform", keyFileVariable: 'keyfile')
						]) {
							sh """#!/bin/bash
							set -ex

							eval `ssh-agent`
							ssh-add \$keyfile

							export PATH=$PATH:$WORKSPACE/metaform/
							cd $WORKSPACE/$REPO_NAME/infrastructure/cloudwatch-log-group

							[ -d ".terraform" ] && rm .terraform -r
							export TF_WARN_OUTPUT_ERRORS=1

							mf init --env $ENV --region $REGION --customer "\$CUSTOMER" -input=false
							mf destroy -input=false -auto-approve
							""".stripIndent().trim()
						}
					}
				}
			}
		}
		stage('Destroy the ASM'){
			steps {
				withCredentials([
				sshUserPrivateKey(credentialsId: "git-metaform", keyFileVariable: 'keyfile'),
				sshUserPrivateKey(credentialsId: "git-devops-infra", keyFileVariable: 'git_devops_infra_key')
				]) {
					sh """#!/bin/bash
					set -ex

					eval `ssh-agent`
					ssh-add \$keyfile

					export PATH=$PATH:$WORKSPACE/metaform/
					cd $WORKSPACE/$REPO_NAME/infrastructure/secret-manager

					[ -d ".terraform" ] && rm .terraform -r
					export TF_WARN_OUTPUT_ERRORS=1

					mf init --env $ENV --region $REGION --customer "\$CUSTOMER" -input=false
					mf destroy -input=false -auto-approve
					""".stripIndent().trim()
				}
			}
		}
	}
	post {
        success {
            slackSend(
                baseUrl: "${SLACK_BASE_URL}", 
                tokenCredentialId: "${SLACK_TOKEN_CREDENTIAL_ID}", 
                color: '#04B404', 
                channel: "${SLACK_NOTIFICATION_CHANNEL}", 
                message: """DEPLOYER: $user \n AWS ACCOUNT: ${AWS_ACCOUNT} \n SUCCESS: ${env.JOB_NAME} \n BUILD NUMBER: [#${env.BUILD_NUMBER}] \n MORE INFO AT: ${env.BUILD_URL}""")
        }
        failure {
            slackSend(
                baseUrl: "${SLACK_BASE_URL}",
                tokenCredentialId: "${SLACK_TOKEN_CREDENTIAL_ID}", 
                color: '#B40404', 
                channel: "${SLACK_NOTIFICATION_CHANNEL}", 
                message: """DEPLOYER: $user \n AWS ACCOUNT: ${AWS_ACCOUNT} \n FAILURE: ${env.JOB_NAME} \n BUILD NUMBER: [#${env.BUILD_NUMBER}] \n MORE INFO AT: (${env.BUILD_URL})""")
        }
        aborted {
            slackSend(
                baseUrl: "${SLACK_BASE_URL}", 
                tokenCredentialId: "${SLACK_TOKEN_CREDENTIAL_ID}", 
                color: '#FF8000', 
                channel: "${SLACK_NOTIFICATION_CHANNEL}", 
                message: """DEPLOYER: $user \n AWS ACCOUNT: ${AWS_ACCOUNT} \n ABORTED: ${env.JOB_NAME} \n BUILD NUMBER: [#${env.BUILD_NUMBER}] \n MORE INFO AT: (${env.BUILD_URL})""")
        }
    }
}