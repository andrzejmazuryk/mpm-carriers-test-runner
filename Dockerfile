FROM openjdk:8-jdk-alpine

EXPOSE 80

# .NET Core dependencies
RUN apk add --no-cache \
        ca-certificates \
        krb5-libs \
        libgcc \
        libintl \
        libssl1.1 \
        libstdc++ \
        lttng-ust \
        tzdata \
        userspace-rcu \
        curl \
        zlib

# Enable detection of running in a container
ENV DOTNET_RUNNING_IN_CONTAINER=true \
# Set the invariant mode since icu_libs isn't included (see https://github.com/dotnet/announcements/issues/20)
DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=true

# Install .NET Core
COPY dotnet.tar.gz /

RUN mkdir -p /usr/share/dotnet \
    && tar -C /usr/share/dotnet -xzf dotnet.tar.gz \
    && ln -s /usr/share/dotnet/dotnet /usr/bin/dotnet \
    && rm dotnet.tar.gz

# Set working directory
ENV WorkingDirectory /tmp/testrunner
ENV SoapUITestsDirectory $WorkingDirectory/soapuitests
ENV HealthCheckDirectory $WorkingDirectory/healthcheck

RUN mkdir $WorkingDirectory
RUN mkdir $SoapUITestsDirectory
RUN mkdir $HealthCheckDirectory

# Install required stuff
RUN apk update && apk add \
    bash \
    git \
    git-lfs \
    lighttpd \
    ca-certificates \
    groff \
    less \
    python \
    py-pip \
    && pip install pip --upgrade \
    && pip install awscli \
    && rm -rf /var/cache/apk/*
    
# Copy required files
COPY start.sh $WorkingDirectory/
COPY healthcheck/health.sh $HealthCheckDirectory/
COPY runtests.sh $WorkingDirectory/
COPY setProperties.sh $WorkingDirectory/
COPY SoapUI.tar.gz $WorkingDirectory/
COPY Activator.xml $SoapUITestsDirectory/
COPY Project.properties $SoapUITestsDirectory/
COPY ActivateModules.xml $SoapUITestsDirectory/
COPY GetActivationCode.xml $SoapUITestsDirectory/
COPY publish/ $SoapUITestsDirectory/publish/

RUN chmod +x $WorkingDirectory/start.sh
# Execute script
ENTRYPOINT /bin/bash -c $WorkingDirectory/start.sh