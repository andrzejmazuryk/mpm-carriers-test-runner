SoapUIPropertiesFileName=Project.properties
if [ -f "$SoapUIPropertiesFileName" ] ; then
    echo "Debug: Update file $SoapUIPropertiesFileName with properties."
    
    if [ ! -z "$MPMUSER" ] ; then
        echo "Debug: MPMUSER set to: $MPMUSER"
        sed -i "/MPMUSER=/ s/=.*/=${MPMUSER}/" $SoapUIPropertiesFileName
    fi

    if [ ! -z "$MPMPASSWORD" ] ; then
    echo "Debug: MPMPASSWORD set to: $MPMPASSWORD"
    sed -i "/MPMPASSWORD=/ s/=.*/=${MPMPASSWORD}/" $SoapUIPropertiesFileName
    fi

    if [ ! -z "$MPMENDPOINT" ] ; then
    echo "Debug: MPMENDPOINT set to: $MPMENDPOINT"
    sed -i "s+MPMENDPOINT=.*+MPMENDPOINT=$MPMENDPOINT+g" $SoapUIPropertiesFileName
    fi

    if [ ! -z "$MPMDB_URL" ] ; then
    echo "Debug: MPMDB_URL set to: $MPMDB_URL"
    sed -i "/MPMDB_URL=/ s/=.*/=${MPMDB_URL}/" $SoapUIPropertiesFileName
    fi

    if [ ! -z "$MPMDB_PORT" ] ; then
    echo "Debug: MPMDB_PORT set to: $MPMDB_PORT"
    sed -i "/MPMDB_PORT=/ s/=.*/=${MPMDB_PORT}/" $SoapUIPropertiesFileName
    fi

    if [ ! -z "$MPMDB_USER" ] ; then
    echo "Debug: MPMDB_USER set to: $MPMDB_USER"
    sed -i "/MPMDB_USER=/ s/=.*/=${MPMDB_USER}/" $SoapUIPropertiesFileName
    fi

    if [ ! -z "$MPMDB_PASSWORD" ] ; then
    echo "Debug: MPMDB_PASSWORD set to: $MPMDB_PASSWORD"
    sed -i "/MPMDB_PASSWORD=/ s/=.*/=${MPMDB_PASSWORD}/" $SoapUIPropertiesFileName
    fi

    if [ ! -z "$MPMDB_DATABASE" ] ; then
    echo "Debug: MPMDB_DATABASE set to: $MPMDB_DATABASE"
    sed -i "/MPMDB_DATABASE=/ s/=.*/=${MPMDB_DATABASE}/" $SoapUIPropertiesFileName
    fi

    if [ ! -z "$REPORTINGDB_URL" ] ; then
    echo "Debug: REPORTINGDB_URL set to: $REPORTINGDB_URL"
    sed -i "/REPORTINGDB_URL=/ s/=.*/=${REPORTINGDB_URL}/" $SoapUIPropertiesFileName
    fi

    if [ ! -z "$REPORTINGDB_USER" ] ; then
    echo "Debug: REPORTINGDB_USER set to: $REPORTINGDB_USER"
    sed -i "/REPORTINGDB_USER=/ s/=.*/=${REPORTINGDB_USER}/" $SoapUIPropertiesFileName
    fi

    if [ ! -z "$REPORTINGDB_PASSWORD" ] ; then
    echo "Debug: REPORTINGDB_PASSWORD set to: $REPORTINGDB_PASSWORD"
    sed -i "/REPORTINGDB_PASSWORD=/ s/=.*/=${REPORTINGDB_PASSWORD}/" $SoapUIPropertiesFileName
    fi

    if [ ! -z "$REPORTINGDB_PORT" ] ; then
    echo "Debug: REPORTINGDB_PORT set to: $REPORTINGDB_PORT"
    sed -i "/REPORTINGDB_PORT=/ s/=.*/=${REPORTINGDB_PORT}/" $SoapUIPropertiesFileName
    fi

    if [ ! -z "$REPORTINGDB_DATABASE" ] ; then
    echo "Debug: REPORTINGDB_DATABASE set to: $REPORTINGDB_DATABASE"
    sed -i "/REPORTINGDB_DATABASE=/ s/=.*/=${REPORTINGDB_DATABASE}/" $SoapUIPropertiesFileName
    fi

    if [ ! -z "$dbLogJobIdGuid" ] ; then
    echo "Debug: DbLogJobID set to: $dbLogJobIdGuid"
    sed -i "/DbLogJobID=/ s/=.*/=${dbLogJobIdGuid}/" $SoapUIPropertiesFileName
    fi

    if [ ! -z "$timestampJOBID" ] ; then
    timestampJOBID=$(date +%s)
    echo "Debug: DbLogJobID set to: $timestampJOBID"
    sed -i "/DbLogJobID=/ s/=.*/=${timestampJOBID}/" $SoapUIPropertiesFileName
    fi
else
    echo "Warning: File $SoapUIPropertiesFileName with properties doesn't exists!"
fi