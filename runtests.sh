#!/bin/bash 
# Exit when error occurs

function setSoapProjectNameFromFile(){
  # Set property
  repositoryName=$1

  fileWithSoapUIProjectName="$repositoryName/soapuiprojectname"
  if [ ! -f $fileWithSoapUIProjectName ] ; then
    soapUIProjectName=""
  else
    SoapUIProjectFileName=`cat $fileWithSoapUIProjectName`
    # Trim leading and trailing whitespaces
    soapUIProjectName=`echo "$SoapUIProjectFileName" | sed -e 's/^ *//g;s/ *$//g'`
  fi
}

function runSoapProject(){
  # Set properties
  projectName=$1
  repositoryName=$2
  SoapUITestRunner=$3
  shouldCopyProperties=$4
  enableLogs=$5
  false=0

  echo "Debug: Run tests for project name: $projectName"
  echo "Debug: shouldCopyProperties: $shouldCopyProperties"
  echo "To not copy those properties it need to be set to: $false"

  if [ "$shouldCopyProperties" -ne "$false" ];then
    echo "Debug: Copy file with properties to catalog with repository: $repositoryName"
    cp -v Project.properties $repositoryName
  fi
  
  # Read soapui project
  if [ ! -d "$repositoryName" ]
  then
      echo "Error: Catalog $repositoryName with project and properties not found. Execute tests aborted!"
  else
      SoapUIProjectFile="$repositoryName/$projectName.xml"
      echo "Debug: SoapUIProjectFile = $SoapUIProjectFile"
      if [ ! -f "$SoapUIProjectFile" ]
      then
          echo "Error: Project file $SoapUIProjectFile doesn't exists! Execute tests aborted!"
      else
          # Execute tests
          if [ -f "$SoapUITestRunner" ]
          then
            echo "Debug: enableLogs: $enableLogs, false: $false"
            if [ "$enableLogs" == "$false" ];then
              echo "Debug: Start SoapUI without logs"
              $SoapUITestRunner $SoapUIProjectFile > /dev/random 2>&1
            else 
              echo "Debug: Start SoapUI"
              $SoapUITestRunner $SoapUIProjectFile
            fi
            echo "Debug: Finish SoapUI"
          else
            echo "Error: File $SoapUITestRunner doesn't exists! Execute tests aborted!"
          fi
      fi
  fi
}

function runSoapProjectIfExists(){
  # Set properties
  gitRepoName=$1
  SoapUITestRunner=$2

  getRepositoryDirectory $gitRepoName
  setSoapProjectNameFromFile $repositoryName
  if [ ! -z $soapUIProjectName ] ; then
    # 0 - dont copy, 1 - copy 
    # 0 - no logs, 1 - logging 
    runSoapProject $soapUIProjectName $repositoryName $SoapUITestRunner 1 1
  else
    echo "Error: File 'soapuiprojectname' doesn't exist! Execute tests aborted!"
  fi
}

function getRepositoryDirectory(){
  # Set property
  gitRepositoryName=$1

  projectDirFileName=$(basename $gitRepositoryName)
  repositoryName=${projectDirFileName%.*}
}

function GuidExist(){
  TIMEOUT=5
  ENDPOINT=http://reporter.$env.aws.metapack.io/reporter/report/$dbLogJobIdGuid
  echo "ENDPOINT: $ENDPOINT"

  HEALTHCHECK_RESPONSE_HTTP_CODE=$(curl --connect-timeout $TIMEOUT -s -o /dev/null -w '%{http_code}' -X GET -H 'Content-Type: plain/text' $ENDPOINT)
  echo "HEALTHCHECK_RESPONSE_HTTP_CODE: $HEALTHCHECK_RESPONSE_HTTP_CODE"

  if [ "$HEALTHCHECK_RESPONSE_HTTP_CODE" != "500" ]; then
    sleep 10000
  fi
}

# =========================================== START SCRIPT ===========================================
set -e

# Unpack SoapUI
echo "Debug: Unpack SoapUI"
SoapUIPath=$WorkingDirectory/SoapUI
SoapUITestRunner=$SoapUIPath/bin/testrunner.sh
echo "Debug: SoapUIPath = $SoapUIPath"
echo "Debug: SoapUITestRunner = $SoapUITestRunner"

mkdir $SoapUIPath
tar -zxvf $WorkingDirectory/SoapUI.tar.gz -C $SoapUIPath

echo "Debug: Go to directory: $SoapUITestsDirectory"
cd $SoapUITestsDirectory

echo "Debug: Run script 'setProperties.sh'"
# Set properties
bash $WorkingDirectory/setProperties.sh

if [ "$GIT_USE_LOCAL_REPO" = true ] ; then
  echo "Debug: Use local git repository - Start"
  cp -r $GIT_LOCAL_REPO_PATH $SoapUITestsDirectory
  export gitRepositoryName=$GIT_LOCAL_REPO_PATH
  echo "Debug: Use local git repository - End"

  runSoapProjectIfExists $gitRepositoryName $SoapUITestRunner
elif [ ! -z $git_repo_name ] ; then 
  echo "Debug: Clone repo from bitbucket: $git_repo_name - Start"
  git clone https://$git_user:$git_password@bitbucket.org/metapack/$git_repo_name
  echo "Debug: Clone repo from bitbucket - End"
 
  runSoapProjectIfExists $git_repo_name $SoapUITestRunner
else
  #Work around which will be removed when all part of flow will work correctly
  #or proposes a new approach
  GuidExist

  apppassword=$(aws secretsmanager get-secret-value --secret-id ${env}_eu-west-1_mpm_tests_shd_tests_bitbucket_app_password --query 'SecretString' --output text)
  repositoryPath="mpm-integration-tests/Tests"
  echo "Debug: Clone repo from bitbucket: mpm-integration-tests.git - Start"
  GIT_LFS_SKIP_SMUDGE=1 git clone https://terransteam:$apppassword@bitbucket.org/metapack/mpm-integration-tests.git
  echo "Debug: Clone repo from bitbucket - End"

  if [ -z $projects_list ] ; then
    for file in $repositoryPath/*.xml; do
      [ -e "$file" ] || continue
      FILENAME=`basename ${file%%.*}`
      projects_list="${projects_list}${FILENAME};"
    done
  fi
  
  echo "Debug: Start Activation of modules"
  ActivatorNameProject="Activator"
    # 0 - dont copy, 1 - copy 
    # 0 - no logs, 1 - logging
  runSoapProject $ActivatorNameProject $SoapUITestsDirectory $SoapUITestRunner 0 $enable_soap_logs
  echo "Debug: END Activation of modules"

  echo "Debug: List of projects: $projects_list"
  nameOfProjects=($projects_list)
  IFS=';' read -ra projects <<< "$nameOfProjects"
  for projectName in "${projects[@]}"
  do
    [ -z "$projectName" ] && continue
    echo "Debug: Pull soap project for $projectName - Start"
    git -C $repositoryPath lfs pull --include "$projectName.xml"
    echo "Debug: Pull soap project for $projectName - End"
    # 0 - dont copy, 1 - copy 
    # 0 - no logs, 1 - logging 
    runSoapProject $projectName $repositoryPath $SoapUITestRunner 1 $enable_soap_logs
  done
fi